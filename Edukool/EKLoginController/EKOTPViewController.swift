//
//  EKOTPViewController.swift
//  Edukool
//
//  Created by Aravind on 12/11/19.
//  Copyright © 2019 DCI. All rights reserved.
//

import UIKit

class EKOTPViewController: UIViewController {
    
    @IBOutlet var textFieldOTP: [OTPField]!
    @IBOutlet var labelRemainingTime: UILabel!
    @IBOutlet var labelMobileNumber: UILabel!
    
    @IBOutlet var buttonResendOTP: UIButton!
    
    var mobileNumber = ""
    
    
    var timer : Timer?
    var seconds = 59
    var otpString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldOTP.forEach({ $0.delegate = self; $0.myTextFieldDelegate = self })
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerStart), userInfo: nil, repeats: true)
    }
    
    @objc func timerStart() {
        self.labelRemainingTime.isHidden = false
        let mins = seconds / 60 % 60
        let secs = seconds % 60
        let restTime = (((mins<10) ? "0" : "") + String(mins) + ":" + ((secs<10) ? "0" : "") + String(secs))
        labelRemainingTime.text = restTime
        if (seconds != 0) {
            seconds = seconds - 1
        } else {
            otpString = ""
            self.textFieldOTP.forEach({$0.text = "" } )
            buttonResendOTP.isHidden = false
            timer?.invalidate()
            labelRemainingTime.isHidden = true
        }
    }
    
    @IBAction func resendOTPTapped(_ sender: Any) {
        otpString = ""
        for fields in textFieldOTP {
            fields.text = ""
        }
//        let params = ["phone":registerParams["phone"]!,"email":registerParams["email"]!, "country":registerParams["country"]!] as [String:Any]
//        CMApiHandler.apiInstance.baseApi(viewController: self, url: "sendOTP", params: params, success: { (successData) in
//            do {
//                let json = try JSON(data: successData)
//                print("OTP data \(json)")
//                self.registerParams["otp"] = json["otp"].int
//                self.registerParams["time"] = json["time"].string
        
                self.timer?.invalidate()
                self.buttonResendOTP.isHidden = true
                self.seconds = 59
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerStart), userInfo: nil, repeats: true)
//                let alert = UIAlertController(title: APP_NAME, message: "OTP has been successfully resent to your email and mobile", preferredStyle: UIAlertController.Style.alert)
//                let alertAction = UIAlertAction(title: ALERT_OK, style: .default, handler:nil)
//                alert.addAction(alertAction)
//                self.present(alert, animated: true,completion:nil)
////            } catch {
////
////            }
////        }, failure: { failure in
////            print("OTP FailureData \(failure)")
////        }) { error in
////            self.showAlert(message: error.localizedDescription)
////        }
    }
    
    @IBAction func verifyOTPTapped(_ sender: Any) {
        otpString = ""
        for fields in textFieldOTP {
            otpString += fields.text!
        }
        
        if (otpString.count < 4) {
            self.showAlert(message: "Please enter all OTP values")
            return
        }
        
        let otp = 1234
        
        if (Int(otpString) == otp) {
//            let params = ["firstname":registerParams["firstname"]!, "email":registerParams["email"]!, "mobile":registerParams["mobile"]!, "password":registerParams["password"]!, "confirmpassword":registerParams["confirmpassword"]!, "countryCode":registerParams["countryCode"]!, "country":registerParams["country"]!] as [String:Any]
//            self.registerNewUser(params:params)
            self.showAlert(message: "Entered OTP is \(otpString)")
        } else {
            self.showAlert(message: "Please enter a valid OTP")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class OTPField: UITextField {
    weak var myTextFieldDelegate: MyTextFieldDelegate?
    
    override func deleteBackward() {
        if text?.isEmpty ?? false {
            myTextFieldDelegate?.textFieldDidEnterBackspace(self)
        }
        super.deleteBackward()
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    @IBInspectable var cornerRadius: CGFloat = 10 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    func sharedInit() {
        refreshCorners(value: cornerRadius)
        borderColor()
    }
    func borderColor(){
        layer.borderColor = UIColor.lightBorderColor.cgColor
        layer.borderWidth = 1.0
    }
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

protocol MyTextFieldDelegate: class {
    func textFieldDidEnterBackspace(_ textField: OTPField)
}

extension EKOTPViewController:UITextFieldDelegate,MyTextFieldDelegate{
    func textFieldDidEnterBackspace(_ textField: OTPField) {
        guard let index = textFieldOTP.firstIndex(of: textField) else {
            return
        }
        if index > 0 {
            textFieldOTP[index - 1].becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
    }
    
    
    func textFieldShouldReturnSingle(_ textField: UITextField, newString : String)
    {
        let nextTag: Int = textField.tag + 1
        textField.text = newString
        let nextResponder: UIResponder? = textField.superview?.viewWithTag(nextTag)
        if let nextR = nextResponder
        {
            // Found next responder, so set it.
            nextR.becomeFirstResponder()
        }
        else
        {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
            //callOTPValidate()
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ((textField.text?.count)! < 1  && string.count > 0){
            if(textField.tag == 1){
                textField.superview?.viewWithTag(textField.tag+1)?.becomeFirstResponder()
            }
            if(textField.tag == 2){
                textField.superview?.viewWithTag(textField.tag+1)?.becomeFirstResponder()
            }
            if(textField.tag == 3){
                textField.superview?.viewWithTag(textField.tag+1)?.becomeFirstResponder()
            }
            if(textField.tag == 4){
                textField.resignFirstResponder()
//                textField.superview?.viewWithTag(textField.tag+1)?.becomeFirstResponder()
            }
//            if(textField.tag == 5){
//                textField.superview?.viewWithTag(textField.tag+1)?.becomeFirstResponder()
//            }
//            if(textField.tag == 6){
//                //textField.superview?.viewWithTag(textField.tag+1)?.becomeFirstResponder()
//                textField.resignFirstResponder()
//                //callOTPValidate()
//            }
            textField.text = string
            return false
            
        }else if ((textField.text?.count)! >= 1  && string.count == 0){
            // on deleting value from Textfield
            if(textField.tag == 1){
                //textField.superview?.viewWithTag(textField.tag+1)?.becomeFirstResponder()
            }
            if(textField.tag == 2){
                textField.superview?.viewWithTag(textField.tag-1)?.becomeFirstResponder()
            }
            if(textField.tag == 3){
                textField.superview?.viewWithTag(textField.tag-1)?.becomeFirstResponder()
            }
            if(textField.tag == 4){
//                textField.superview?.viewWithTag(textField.tag-1)?.becomeFirstResponder()
            }
//            if(textField.tag == 5){
//                textField.superview?.viewWithTag(textField.tag-1)?.becomeFirstResponder()
//            }
//            if(textField.tag == 6){
//                //textField.superview?.viewWithTag(textField.tag+1)?.becomeFirstResponder()
//            }
            textField.text = ""
            return false
        }else if ((textField.text?.count)! >= 1  ){
            textField.text = string
            return false
        }
        return true
        
    }
}

