//
//  EKLoginViewController.swift
//  Edukool
//
//  Created by Aravind on 08/11/19.
//  Copyright © 2019 DCI. All rights reserved.
//

import UIKit
import SKCountryPicker

class EKLoginViewController: UIViewController {
    
    var checkUserData: EKCheckUserBase?
    
    @IBOutlet var viewPhoneNumber: UIView!{
        didSet{
            self.viewPhoneNumber.layer.borderWidth = 0.5
            self.viewPhoneNumber.layer.borderColor = UIColor.lightBorderColor.cgColor
        }
    }
    @IBOutlet var textFieldPhoneNumber: UITextField!{
        didSet{
            self.textFieldPhoneNumber.delegate = self
        }
    }
    @IBOutlet var imageViewCountry: UIImageView!
    @IBOutlet var labelCountry: UILabel!
    var selectedCountry: Country?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let country = CountryManager.shared.currentCountry else {
            self.labelCountry.text = ""
//            self.textFieldCountry.placeholder = "+"
            self.imageViewCountry.isHidden = true
            return
        }
        
        labelCountry.text = "(\(country.countryCode )) \(country.dialingCode ?? "")"
        imageViewCountry.image = country.flag
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.tabBarController?.tabBar.isHidden = true
    }

    @IBAction func countryButtonTapped(_ sender: Any) {
        
        // Invoke below static method to present country picker without section control
        // CountryPickerController.presentController(on: self) { ... }
        
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
            
            guard let self = self else { return }
            self.selectedCountry = country
            self.imageViewCountry.image = country.flag
            self.labelCountry.text = "(\(country.countryCode)) \(country.dialingCode ?? "")"
            
            print("\(country.countryCode) \n (\(country.countryName) \n \(String(describing: country.dialingCode)) \n \(String(describing: country.digitCountrycode))) \n \(String(describing: country.flag))")
            
            print("Selected Country \(self.selectedCountry?.countryName ?? "")")
        }
        
        // can customize the countryPicker here e.g font and color
        countryController.detailColor = UIColor.primaryColor
        
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        print("Phone Number\(String(describing: textFieldPhoneNumber.text))")
        if (textFieldPhoneNumber.text == "" || (textFieldPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!) {
            self.showAlert(message: "Please enter valid phone number")
        } else {
            var params = [String : Any]()
            params = ["phoneNumber" : "\(textFieldPhoneNumber.text ?? "")", "headerType" : "1"]
            
            EKApiHandler.apiInstance.baseApi(viewController: self, url: "checkuserExists", params: params, success: { (successData) in
                print("Check user successData \(successData)")
                
                do{
                    let apiResponseData = try JSONDecoder().decode(EKCheckUserBase.self, from: successData)
                    self.checkUserData = apiResponseData
                    if(self.checkUserData != nil) {
                        self.checkUserStateProcess(userData: self.checkUserData!)
                    }
                } catch {
                    
                }
                
//                let spVC : EKSecurityPinController = UIStoryboard(storyboard: .main, bundle: nil).instantiateViewController()
//                self.navigationController?.pushViewController(spVC, animated: false)
            }, failure: { (failureData) in
                print("Check user failureData \(failureData)")
            }) { (errorData) in
                print("Check user errorData \(errorData)")
            }
        }
    }
    
    func checkUserStateProcess(userData: EKCheckUserBase) {
        
        if (userData.securityPin == "") {
            self.showAlert(message: "Your security PIN is empty")
        } else if (userData.securityPin != "" && userData.appSettings?[0].securityPinStatus == 0) {
            self.showAlert(message: "Sorry the security PIN verification is disabled")
            let appDelegate: AppDelegate = AppDelegate().sharedInstance()
            appDelegate.currentState = 1
            appDelegate.checkUserLoginStatus()
        } else {
//            let spVC : EKSecurityPinController = UIStoryboard(storyboard: .main, bundle: nil).instantiateViewController()
//            spVC.securityPin = userData.securityPin ?? ""
//            self.navigationController?.pushViewController(spVC, animated: false)
            
            let otpVC: EKOTPViewController = UIStoryboard(storyboard: .main, bundle: nil).instantiateViewController()
            otpVC.mobileNumber = "\(selectedCountry?.countryCode ?? "")\(textFieldPhoneNumber.text ?? "")"
            self.navigationController?.pushViewController(otpVC, animated: false)
            
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EKLoginViewController: UITextFieldDelegate {
    
}
