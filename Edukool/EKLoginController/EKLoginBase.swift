//
//  EKLoginBase.swift
//  Edukool
//
//  Created by Aravind on 11/11/19.
//  Copyright © 2019 DCI. All rights reserved.
//

import Foundation

// MARK: - EKCheckUserBase
class EKCheckUserBase: Codable {
    let statusText, tokenParent, securityPin, phoneNumber: String?
    let message: String?
    let appSettings: [AppSetting]?
    let userExist, status: Int?
    
    init(statusText: String?, tokenParent: String?, securityPin: String?, phoneNumber: String?, message: String?, appSettings: [AppSetting]?, userExist: Int?, status: Int?) {
        self.statusText = statusText
        self.tokenParent = tokenParent
        self.securityPin = securityPin
        self.phoneNumber = phoneNumber
        self.message = message
        self.appSettings = appSettings
        self.userExist = userExist
        self.status = status
    }
}

// MARK: - AppSetting
class AppSetting: Codable {
    let messageStatus, securityPinStatus, userType, notificationStatus: Int?
    
    enum CodingKeys: String, CodingKey {
        case messageStatus = "MessageStatus"
        case securityPinStatus = "SecurityPinStatus"
        case userType = "UserType"
        case notificationStatus = "NotificationStatus"
    }
    
    init(messageStatus: Int?, securityPinStatus: Int?, userType: Int?, notificationStatus: Int?) {
        self.messageStatus = messageStatus
        self.securityPinStatus = securityPinStatus
        self.userType = userType
        self.notificationStatus = notificationStatus
    }
}

