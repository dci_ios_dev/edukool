//
//  EKSecurityPinController.swift
//  Edukool
//
//  Created by Aravind on 09/11/19.
//  Copyright © 2019 DCI. All rights reserved.
//

import UIKit

class EKSecurityPinController: UIViewController {
    
    @IBOutlet var textFieldSecurityPin: EKTextField!
    var securityPin = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        self.view.endEditing(true)
        if(textFieldSecurityPin.text == securityPin){
            let appDelegate: AppDelegate = AppDelegate().sharedInstance()
            appDelegate.currentState = 1
            appDelegate.checkUserLoginStatus()
        } else {
            let otpVC: EKOTPViewController = UIStoryboard(storyboard: .main, bundle: nil).instantiateViewController()
            self.navigationController?.pushViewController(otpVC, animated: false)
//            self.showAlert(message: "Invalid Security PIN")
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
