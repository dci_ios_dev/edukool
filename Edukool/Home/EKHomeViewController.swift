//
//  EKHomeViewController.swift
//  Edukool
//
//  Created by Aravind on 07/11/19.
//  Copyright © 2019 DCI. All rights reserved.
//

import UIKit

class EKHomeViewController: UIViewController {
    @IBOutlet weak var childrenCollectionView: UICollectionView!
    @IBOutlet weak var homeTableView: UITableView!
    var isReminderCloseTapped = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for family: String in UIFont.familyNames {
            print(family)
            for names: String in UIFont.fontNames(forFamilyName: family) {
                print("== \(names)")
            }
        }
        self.navigationController?.navigationBar.isHidden = true
        homeTableView.tableFooterView = UIView()
        homeTableView.register(UINib(nibName: "EKHomeProgressTableCell", bundle: nil), forCellReuseIdentifier: "EKHomeProgressTableCell")
        homeTableView.register(UINib(nibName: "EKHomeworkTableCell", bundle: nil), forCellReuseIdentifier: "EKHomeworkTableCell")
        childrenCollectionView.flashScrollIndicators()
    }
    
    @IBAction func notificationTapped() {
        
    }
    
    @IBAction func searchTapped() {
        
    }
    
    @objc func reminderCloseTapped() {
        isReminderCloseTapped = true
//        let indexPath = IndexPath(row: 1, section: 0)
//        homeTableView.beginUpdates()
//        homeTableView.deleteRows(at:[indexPath] , with: .fade)
//        homeTableView.endUpdates()
    }
}

extension EKHomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! EKHomeCollectionViewCell
        cell.configView(Name: "Kalidasan", imagePath: "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension EKHomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if (indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EKSchoolCell", for: indexPath) as! EKSchoolCell
            return cell
        } else if (indexPath.row == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EKReminderCell", for: indexPath) as! EKReminderCell
            cell.reminderCloseButton.addTarget(self, action: #selector(reminderCloseTapped), for: .touchUpInside)
            return cell
        } else if (indexPath.row == 2) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EKHomeProgressTableCell", for: indexPath) as! EKHomeProgressTableCell
            return cell
        } else if (indexPath.row == 3) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EKHomeworkTableCell", for: indexPath) as! EKHomeworkTableCell
            return cell
        }
        return cell
    }
}

class EKSchoolCell: UITableViewCell {
}

class EKReminderCell: UITableViewCell {
    @IBOutlet var reminderCloseButton: UIButton!
}
