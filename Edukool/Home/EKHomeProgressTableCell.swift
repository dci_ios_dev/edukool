//
//  EKHomeProgressTableCell.swift
//  CrowdMaestro
//
//  Copyright © 2019 DCI. All rights reserved.
//

import UIKit

class EKHomeProgressTableCell: UITableViewCell {
    @IBOutlet var progressView:EKCircularProgressView!
    @IBOutlet var progressLabel:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        progressView.trackColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
        progressView.progressColor = UIColor(red: 38/255, green: 128/255, blue: 235/255, alpha: 1.0)
        progressView.setProgressWithAnimation(duration: 0.0, value: 0.827)
        progressLabel.text = "82.7 %"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
