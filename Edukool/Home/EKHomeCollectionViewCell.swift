//
//  EKHomeCollectionViewCell.swift
//  CollectionView
//
//

import UIKit

class EKHomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet var labelStudentName: UILabel!
    @IBOutlet var labelClassName: UILabel!
    @IBOutlet var labelTeacherName: UILabel!
    @IBOutlet weak var studentImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configView(Name:String , imagePath: String) {
        labelStudentName.text = Name
        labelClassName.text = "Class 2 A"
        labelTeacherName.text = "Ms. Mercy D'souza"
    }
}
