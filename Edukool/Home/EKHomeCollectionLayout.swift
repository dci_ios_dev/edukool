
import UIKit

private let pageWidth: CGFloat = UIScreen.main.bounds.width
private let pageHeight: CGFloat = 102

class EKHomeCollectionLayout: UICollectionViewFlowLayout, UICollectionViewDelegateFlowLayout {
	
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        scrollDirection = UICollectionView.ScrollDirection.horizontal
        minimumInteritemSpacing = 0
        minimumLineSpacing = 0
	}
	
    override func prepare() {
		super.prepare()
        collectionView?.decelerationRate = UIScrollView.DecelerationRate.fast
        collectionView?.contentInset = UIEdgeInsets(
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        )
        itemSize = CGSize(width: pageWidth, height: pageHeight)
        collectionView?.isPagingEnabled = true
	}
}
