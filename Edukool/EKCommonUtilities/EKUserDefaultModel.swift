//
//  EKUserDefaultModel.swift
//  Edukool
//
//  Created by Aravind on 09/11/19.
//  Copyright © 2019 DCI. All rights reserved.
//

import Foundation

class EKUserDefaultModel: NSObject {
    static let sharedInstance = EKUserDefaultModel()
    
    func setAuthorizationHeader(header: String) {
        UserDefaults.standard.set(header, forKey: "authorization")
    }
    
    func getAuthorizationHeader() -> String? {
        return UserDefaults.standard.value(forKey: "authorization") as? String
    }
}
