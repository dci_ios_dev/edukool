//
//  EKConstants.swift
//  Edukool
//
//  Created by Aravind on 09/11/19.
//  Copyright © 2019 DCI. All rights reserved.
//

import Foundation

//MARK:- API Calls
let BASE_URL = "http://stagingerp.edukool.com/api/"
let HEADER_VALUE = "U2VjcmV0OnBhbmR0YXV0aGN8UGFzc3dvcmQ6IyRlZnJIeWhhNjQ3"

//MARK:- Alerts
var NETWORK_ALERT = "Please check your Internet Connection"

//MARK:- API Status
let STATUS_SUCCESS = 200
let STATUS_TOKENEXPIRE = 401
let STATUS_ERROR = 402
let NORESULT = 400
