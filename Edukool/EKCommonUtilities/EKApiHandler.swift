//
//  EKApiHandler.swift
//  Edukool
//
//  Created by Aravind on 09/11/19.
//  Copyright © 2019 DCI. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

// For handling API Calls
class EKApiHandler: NSObject {
    
    static let apiInstance = EKApiHandler()
    
    // Common API to handle all api requests
    func baseApi(viewController:UIViewController,url:String,params:[String:Any],success: @escaping(Data)->Void,failure:@escaping(Int)->Void,error:@escaping(Error) -> Void){
        DispatchQueue.main.async {
            LoadingIndicator.shared.show()
        }
        var headers = ["Authorization": "\(EKUserDefaultModel().getAuthorizationHeader() ?? "")"]
        headers["WSH"] = HEADER_VALUE
        print("Headers:\(headers)")
        if NetworkReachabilityManager()!.isReachable{
            print("URL:\(BASE_URL+url),Params:\(params)")
            let queue = DispatchQueue(label: "Queue", qos: .userInteractive, attributes: [.concurrent])
            
            Alamofire.request(BASE_URL+url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).response (
                queue: queue,
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    DispatchQueue.main.async {
                        LoadingIndicator.shared.hide()
                        switch response.result{
                        case .success:
                            if response.data != nil {
                                do{
                                    let json = try JSON(data: response.data!)
                                    let bcf = ByteCountFormatter()
                                    bcf.allowedUnits = [.useKB] // optional: restricts the units to MB only
                                    bcf.countStyle = .file
                                    let string = bcf.string(fromByteCount: Int64(response.data!.count))
                                    print("\(url) Response Size \(string) in KB")
                                    print("Base Api JSON \(json)")
                                    if (json["status"].int == NORESULT){
                                        failure(json["status"].int!)
                                        
//                                        if !(viewController.isKind(of: CMMainViewController.self)) {
//                                            viewController.showAlert(message: json["message"].string!)
//                                        }
                                    } else if(json["status"].int == STATUS_TOKENEXPIRE) {
                                        EKUserDefaultModel().setAuthorizationHeader(header: response.response?.allHeaderFields["Authorization"] as? String ?? "")
                                        failure(json["status"].int!)
                                    }
                                    else if (json["status"].int == STATUS_SUCCESS){
                                        if (url == "checkuserExists"){
                                            EKUserDefaultModel().setAuthorizationHeader(header: json["tokenParent"].string ?? "")
                                        }
                                        success(response.data!)
                                    } else if (json["status"].int == STATUS_ERROR) {
                                        failure(json["status"].int!)
                                        viewController.showAlert(message: json["message"].string!)
                                    }
//                                    else if (json["status"] == 405) {
//                                        failure(json["status"].int!)
//
//                                        let alert = UIAlertController(title: APP_NAME, message: "You are not allowed to continue since you are inactive", preferredStyle: UIAlertController.Style.alert)
//                                        let alertAction = UIAlertAction(title: ALERT_OK, style: .default, handler: self.logout(action:))
//                                        alert.addAction(alertAction)
//                                        viewController.present(alert, animated: true,completion:nil)
//                                    }
                                    else {
                                        failure(json["status"].int!)
                                    }
                                }catch{
                                }
                            }
                            break
                        case .failure:
                            error(response.result.error!)
                            viewController.showAlert(message:"Something went wrong. Please try after sometime.")
                            break
                        }
                    }
            })
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                LoadingIndicator.shared.hide()
                viewController.showAlert(message: NETWORK_ALERT)
            }
        }
    }
    
}
