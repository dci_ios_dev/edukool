//
//  EKHomeworkViewController.swift
//  Edukool
//
//  Created by Kalidasan on 08/11/19.
//  Copyright © 2019 DCI. All rights reserved.
//

import UIKit

class EKHomeworkViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configNavView()
    }
    
    func configNavView() {
        let rightNotificationBarButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav-notification"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(notificationTapped))
        let rightSearchBarButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav-search"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(searchTapped))
        self.navigationItem.setRightBarButtonItems([rightNotificationBarButtonItem,rightSearchBarButtonItem], animated: false)
        let logo = UIBarButtonItem(image: UIImage (named: "nav-logo"), style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem = logo
    }
    
    @objc func notificationTapped() {
        
    }
    
    @objc func searchTapped() {
        
    }
}
